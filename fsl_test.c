
#include <stdio.h>
#include <syslog.h>
#include <fcntl.h>  /* open(2) */
#include <unistd.h> /* close(2) */
#include <stdlib.h> /* exit(3) */

static void pexit(const char *msg, int rc)
{
    fprintf(stderr, "ERROR: %s\n", msg);
    exit(rc);
}

static int tryfd(void)
{
    int fd;

    if ((fd = open("/dev/null", O_RDONLY)) == -1)
        pexit("internal - cannot open /dev/null for reading", 1);
    if (close(fd) == -1)
        pexit("internal - cannot close(2)", 1);
    return fd;
}

int main(int argc, char *argv[])
{
    int fd1, fd2;

    fd1 = tryfd();
    setlogmask(LOG_UPTO(LOG_ERR));
    openlog("mail", LOG_PID, LOG_LOCAL0); /* this must not consume a filedescriptor; LOG_NDELAY and break it */
    setlogmask(LOG_UPTO(LOG_ERR));
    fd2 = tryfd();
    if (fd1 != fd2)
        pexit("testsuite - filedescriptor allocated where it should not", 2);
    closelog();

    fd1 = tryfd();
    openlog("mail", LOG_PID, LOG_LOCAL1);
    if (fd1 != tryfd())
        pexit("testsuite - filedescriptor allocated where it should not", 2);
    setlogmask(LOG_UPTO(LOG_ERR));
    if (fd1 != tryfd())
        pexit("testsuite - filedescriptor allocated where it should not", 2);
    syslog(LOG_ALERT, "one");
    syslog(LOG_ALERT, "two");
    closelog();

    openlog("mail", LOG_PID|LOG_NDELAY, LOG_LOCAL0);
    syslog(LOG_ALERT, "who: internal 23\n");
    setlogmask(LOG_UPTO(LOG_ERR));
    syslog(LOG_INFO|LOG_LOCAL2, "foobar: %m");
    syslog(LOG_EMERG,   "syslogging LOG_EMERG   %d", LOG_EMERG  );
    syslog(LOG_ALERT,   "syslogging LOG_ALERT   %d", LOG_ALERT  );
    syslog(LOG_CRIT,    "syslogging LOG_CRIT    %d", LOG_CRIT   );
    syslog(LOG_ERR,     "syslogging LOG_ERR     %d", LOG_ERR    );
    syslog(LOG_WARNING, "syslogging LOG_WARNING %d", LOG_WARNING);
    syslog(LOG_NOTICE,  "syslogging LOG_NOTICE  %d", LOG_NOTICE );
    syslog(LOG_INFO,    "syslogging LOG_INFO    %d", LOG_INFO   );
    syslog(LOG_DEBUG,   "syslogging LOG_DEBUG   %d", LOG_DEBUG  );
    closelog();

    return 0;
}









/*
**  fsl_version.c -- Version Information for OSSP fsl (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _FSL_VERSION_C_AS_HEADER_

#ifndef _FSL_VERSION_C_
#define _FSL_VERSION_C_

#define FSL_VERSION 0x107200

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} fsl_version_t;

extern fsl_version_t fsl_version;

#endif /* _FSL_VERSION_C_ */

#else /* _FSL_VERSION_C_AS_HEADER_ */

#define _FSL_VERSION_C_AS_HEADER_
#include "fsl_version.c"
#undef  _FSL_VERSION_C_AS_HEADER_

fsl_version_t fsl_version = {
    0x107200,
    "1.7.0",
    "1.7.0 (02-Mar-2007)",
    "This is OSSP fsl, Version 1.7.0 (02-Mar-2007)",
    "OSSP fsl 1.7.0 (02-Mar-2007)",
    "OSSP fsl/1.7.0",
    "@(#)OSSP fsl 1.7.0 (02-Mar-2007)",
    "$Id: fsl_version.c,v 1.38 2007/03/02 17:02:08 thl Exp $"
};

#endif /* _FSL_VERSION_C_AS_HEADER_ */

